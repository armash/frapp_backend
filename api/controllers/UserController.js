/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var jwt    = require('jsonwebtoken');
var config = require('../../config'); // get our config file

module.exports = {

  index : function (req, res, next) {
    // console.log(new Date());
    // console.log(req.session.authenticated);
    var token = req.param('token');
    console.log(token);
    // return 1;
    if (token) {

      // verifies secret and checks exp
      jwt.verify(token, config.secret, function(err, decoded) {
        if (err) {
          return res.json({
            success: false,
            message: 'Failed to authenticate token.'
          });
        } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;
          User.find(function foundUsers(err, users) {
            if(err){
              res.status("400");
              return res.json({ success: false, message: 'Please Enter Valid Information' });
            }

            //PASS ARRAY OF USERS TO INDEX VIEW
            res.status(201);

            res.json(users);

          });
          // next();
        }
      });

    } else {

      // if there is no token
      // return an error
      return res.status(403).send({
        success: false,
        message: 'No token provided.'
      });

    }


  },

  // CREATE ACTION
  create: function(req, res, next)
  {
    var params = req.params.all();

    User.create(params, function (err, user) {
      if(err) return next(err);

      res.status(201);

      res.json(user);

    });
  },

};


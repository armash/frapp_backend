/**
 * SessionController
 *
 * @description :: Server-side logic for managing sessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var bcrypt = require('bcrypt');
var jwt    = require('jsonwebtoken');
var config = require('../../config'); // get our config file

module.exports = {

  'create': function (req, res, next) {
    var password = req.param('password');
    console.log(password);

    //CHECK FOR EMAIL & PASSWORD IF NOT ENTERED REDIRECT BACK TO SIGNIN FORM
    if(!req.param('email') || !req.param('password')) {
      console.log('no valid info');
      res.status("400");
      return res.json({ success: false, message: 'Please Provide Valid Information.' });
    }

    //FIND USER IN DATABASE BY EMAIL
    User.findOneByEmail(req.param('email'), function (err, user) {

      if(err) return next(err);

      //IF NO USER FOUND
      if(!user){
        console.log('user not found');
        res.status("400");
        return res.json({ success: false, message: 'Authentication failed.User Not Found.' });
      }

      bcrypt.compare(password, user.password, function (err, valid) {
        if (err) return next(err);

        //IF PASSWORD DOES NOT MATCH
        if(!valid) {
          console.log('Password Mismatch');
          res.status("400");
          return res.json({ success: false, message: 'Authentication failed.Password Mismatch.' });
        }

        req.session.authenticated = true;
        req.session.User = user;
        console.log('Success');
        // res.status(201);
        // res.json(user);

        var token = jwt.sign(user, config.secret, {
          // expiresInMinutes: 1440 // expires in 24 hours
        });

        // return the information including token as JSON
        res.json({
          success: true,
          message: 'Enjoy your token!',
          token: token
        });


      });
    });
  },

  destroy: function (req, res, next) {
    req.session.destroy();
    res.redirect('/');
  }

};

